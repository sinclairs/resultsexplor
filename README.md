# Welcome to ResultsExplor!

A site for exploring results of my machine learning audio synthesis
models.

This site was implemented in Flask and based on the [Flask Mega-Tutorial](https://blog.miguelgrinberg.com/post/the-flask-mega-tutorial-part-i-hello-world).

It depends on a microservice running on my training machine that
serves the results using Django.
