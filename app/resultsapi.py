
# Interface to the Results API served by Django

import json
import requests
from flask import current_app
from app.models import Result
from flask_babel import _

def get_results_list():
    url = current_app.config['RESULTS_API_URL']
    r = requests.get(url + '/v1/results')
    if r.status_code != 200:
        return _('Error: results API error ')+str(r.status_code)+'.'
    r = json.loads(r.content.decode('utf-8-sig'))
    for x in r:
        x['config'] = json.loads(x['config'])
    return r

def get_result(result_id):
    url = current_app.config['RESULTS_API_URL']
    r = requests.get(url + f'/v1/results/{result_id}/')
    if r.status_code != 200:
        return _('Error: results API error ')+str(r.status_code)+'.'
    r = json.loads(r.content.decode('utf-8-sig'))
    r['config'] = json.loads(r['config'])
    return r
