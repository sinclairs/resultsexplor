
class DynamicPlot {

  constructor(div_selector, divloading_selector, slider_selector, data_dim, decode_url) {
    this.div_selector = div_selector;
    this.divloading_selector = divloading_selector;
    this.data_dim = data_dim;
    this.decode_url = decode_url;
    this.data = new Float32Array(this.data_dim);
    this.update_count = 0;
    this.latent_sliders = [];
    this.timeout = null;
    this.latent_sliders = $.map($(slider_selector).slider(),
                                (v,i)=>v
                                .on('change',()=>this.on_latent_change())
                                .data('slider'));
    if (typeof ResizeObserver == "function") {
      new ResizeObserver((()=>this.do_plot())).observe($(this.div_selector)[0]);
    }
    this.do_plot();
    this.on_latent_change();
  }

  do_plot(args) {
    d3.select("svg").remove(); // for redraw
    var w = parseInt($(this.div_selector)[0].clientWidth);
    var svg = d3.select(this.div_selector)
        .append("svg")
          .attr("width", "100%")
          .attr("height", 300)
        .append("g")
          .attr("transform", "translate(40,25)");
    var x = d3.scaleLinear()
        .domain([0,this.data_dim])
        .range([0,w-60]);
    var y = d3.scaleLinear()
        .domain([-1.2,1.2])
        .range([250,0]);
    svg.append("g")
      .attr("transform", "translate(0,250)")
      .call(d3.axisBottom(x));
    svg.append("g").call(d3.axisLeft(y));
    this.line = d3.line()
        .x((d,i) => x(i))
        .y((d,i) => y(d));
    svg.append("path")
      .datum(this.data)
      .attr("class","line")
      .attr("fill","none")
      .attr("stroke","white")
      .attr("stroke-width",1.5)
      .attr("stroke-linejoin","round")
      .attr("d", this.line);
  }

  update_plot() {
    var svg = d3.select(this.div_selector).transition();
    svg.select(".line").duration(750).attr("d", this.line(this.data));
  }

  update_values() {
    this.timeout = null;
    var latent_query = "[[" + this.latent_params + "]]";
    $.get(this.decode_url + "?latent="+latent_query, {
    }).done(response=>{
      $(this.divloading_selector).remove();
      var d = JSON.parse(response);
      this.data.set(d[0]);
      this.update_plot();
      this.update_count = 0;
    }).fail(e=>{
      if (e.status==403) {
        this.update_count += 1;
        if (this.update_count < 3) {
          this.update_values();
        }
      } else
        console.log('Error requesting decode: ' + e.status + ' ' + e.statusText);
    });
  }

  on_latent_change() {
    this.latent_params = this.latent_sliders.map(l => l.getValue());
    this.update_count=0;
    if (this.timeout == null)
      this.timeout = setTimeout((()=>this.update_values()),500);
  }

}
