
module "lambda_function" {
  source = "terraform-aws-modules/lambda/aws"

  function_name = "ResultsExplor"
  description   = "ResultsExplor (front-end)"
  handler       = "run_lambda.http_server"
  runtime       = "python3.9"
  publish = true

  create_package = "false"
  local_existing_package = "flask-app.zip"

  # source_path = ".."

  allowed_triggers = {
    APIGatewayAny = {
      service    = "apigateway"
      # source_arn = module.api_gateway.apigatewayv2_api_arn
      statement_id  = "AllowExecutionFromAPIGateway"
    }
  }

  policy_statements = [
    {
      actions = [
        "dynamodb:BatchGetItem",
        "dynamodb:BatchWriteItem",
        "dynamodb:ConditionCheckItem",
        "dynamodb:PutItem",
        "dynamodb:DescribeTable",
        "dynamodb:DeleteItem",
        "dynamodb:GetItem",
        "dynamodb:Scan",
        "dynamodb:Query",
        "dynamodb:UpdateItem"
      ]
      effect = "Allow"
      resources = [aws_dynamodb_table.session.arn]
    }
  ]
  attach_policy_statements = true

  tags = {
    Name = "ResultsExplor"
  }
}

module "api_gateway" {
  source = "terraform-aws-modules/apigateway-v2/aws"

  name          = "ResultsExplor-http"
  description   = "ResultsExplor (gateway)"
  protocol_type = "HTTP"

  cors_configuration = {
    allow_headers = ["content-type", "x-amz-date", "authorization", "x-api-key", "x-amz-security-token", "x-amz-user-agent"]
    allow_methods = ["*"]
    allow_origins = ["*"]
  }

  # mutual_tls_authentication = {
  #   truststore_uri     = "s3://${aws_s3_bucket.truststore.bucket}/${aws_s3_bucket_object.truststore.id}"
  #   truststore_version = aws_s3_bucket_object.truststore.version_id
  # }

  # domain_name                 = "terraform-aws-modules.modules.tf"
  # domain_name_certificate_arn = module.acm.acm_certificate_arn
  create_api_domain_name = false

  # default_stage_access_log_destination_arn = aws_cloudwatch_log_group.logs.arn
  # default_stage_access_log_format          = "$context.identity.sourceIp - - [$context.requestTime] \"$context.httpMethod $context.routeKey $context.protocol\" $context.status $context.responseLength $context.requestId $context.integrationErrorMessage"

  # target          = module.lambda_function.lambda_function_arn

  default_route_settings = {
    detailed_metrics_enabled = true
    throttling_burst_limit   = 100
    throttling_rate_limit    = 100
  }

  # authorizers = {
  #   "cognito" = {
  #     authorizer_type  = "JWT"
  #     identity_sources = "$request.header.Authorization"
  #     name             = "cognito"
  #     audience         = ["d6a38afd-45d6-4874-d1aa-3c5c558aqcc2"]
  #     issuer           = "https://${aws_cognito_user_pool.this.endpoint}"
  #   }
  # }

  integrations = {

    "ANY /" = {
      lambda_arn             = module.lambda_function.lambda_function_arn
      payload_format_version = "1.0"
      timeout_milliseconds   = 12000
    }

    "ANY /{proxy+}" = {
      lambda_arn             = module.lambda_function.lambda_function_arn
      payload_format_version = "1.0"
      timeout_milliseconds   = 12000
    }

    "$default" = {
      lambda_arn = module.lambda_function.lambda_function_arn
      payload_format_version = "1.0"
      timeout_milliseconds   = 12000

      # tls_config = jsonencode({
      #   server_name_to_verify = local.domain_name
      # })

      # response_parameters = jsonencode([
      #   {
      #     status_code = 500
      #     mappings = {
      #       "append:header.header1" = "$context.requestId"
      #       "overwrite:statuscode"  = "403"
      #     }
      #   },
      #   {
      #     status_code = 404
      #     mappings = {
      #       "append:header.error" = "$stageVariables.environmentId"
      #     }
      #   }
      # ])
    }

  }

  # body = templatefile("api.yaml", {
  #   example_function_arn = module.lambda_function.lambda_function_arn
  # })

  tags = {
    Name = "dev-api-new"
  }
}

# module "lambda_api_gateway" {
#     source               = "git@github.com:techjacker/terraform-aws-lambda-api-gateway"

#     # tags
#     project              = "ResultsExplor"
#     service              = "acme-corp"
#     owner                = "Roadrunner"
#     costcenter           = "acme-abc"

#     # vpc
#     vpc_cidr             = "10.0.0.0/16"
#     public_subnets_cidr  = ["10.0.1.0/24", "10.0.2.0/24"]
#     private_subnets_cidr = ["10.0.3.0/24", "10.0.4.0/24"]
#     nat_cidr             = ["10.0.5.0/24", "10.0.6.0/24"]
#     igw_cidr             = "10.0.8.0/24"
#     azs                  = ["eu-central-1a", "eu-central-1b"]

#     # lambda
#     lambda_zip_path      = "flask-app.zip"
#     lambda_handler       = "run_lambda.http_server"
#     lambda_runtime       = "python3.8"
#     lambda_function_name = "ResultsExplor"

#     # API gateway
#     region               = "eu-central-1"
#     account_id           = "578209891202"
# }


## For session management and credentials store

resource "aws_dynamodb_table" "session" {
  name = "ResultsExplor-session"
  billing_mode = "PROVISIONED"
  read_capacity= "5"
  write_capacity= "5"
  attribute {
    name = "id"
    type = "S"
  }
  hash_key = "id"
}
