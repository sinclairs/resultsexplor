FROM python:3.6-alpine

RUN adduser -D rsexplor

WORKDIR /home/rsexplor

COPY requirements.txt requirements.txt
RUN python -m venv venv
RUN venv/bin/pip install -r requirements.txt
RUN venv/bin/pip install gunicorn pymysql

COPY app app
COPY migrations migrations
COPY resultsexplor.py config.py boot.sh ./
RUN chmod a+x boot.sh

ENV FLASK_APP resultsexplor.py

RUN chown -R rsexplor:rsexplor ./
USER rsexplor

EXPOSE 5000
ENTRYPOINT ["./boot.sh"]
